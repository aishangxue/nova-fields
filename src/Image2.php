<?php

namespace SC\NovaFields;

use Laravel\Nova\Fields\Field;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class Image2 extends Field
{
    public $showOnIndex = true;
    public $component = 'image2';

    protected $canEditLink = true;
    protected $indexMaxWidth = null;
    protected $maxWidth = 320;

    public function canEditLink($canEditLink=true)
    {
        $this->canEditLink = $canEditLink;
        return $this;
    }

    public function indexMaxWidth($width)
    {
        $this->indexMaxWidth = $width;
        return $this;
    }

    public function maxWidth($width)
    {
        $this->maxWidth = $width;
        return $this;
    }

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'canEditLink' => $this->canEditLink,
            'indexMaxWidth' => $this->indexMaxWidth,
            'maxWidth' => $this->maxWidth,
        ]);
    }
}
