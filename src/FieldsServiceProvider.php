<?php

namespace SC\NovaFields;

use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;
use Illuminate\Support\ServiceProvider;

class FieldsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            Nova::script('textarea2', __DIR__.'/../dist/js/nova-fields.js');
            Nova::style('textarea2', __DIR__.'/../dist/css/nova-fields.css');
            Nova::script('image2', __DIR__.'/../dist/js/nova-fields.js');
            Nova::style('image2', __DIR__.'/../dist/css/nova-fields.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
