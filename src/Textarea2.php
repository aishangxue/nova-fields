<?php

namespace SC\NovaFields;

use Laravel\Nova\Fields\Field;

class Textarea2 extends Field
{
    public $showOnIndex = false;
    public $component = 'textarea2';

    protected $indexMaxWidth = null;

    public function indexMaxWidth($maxWidth)
    {
        $this->showOnIndex = true;
        $this->indexMaxWidth = $maxWidth;

        return $this;
    }

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'indexMaxWidth' => $this->indexMaxWidth,
        ]);
    }
}
