Nova.booting((Vue, router, store) => {
    Vue.component('index-textarea2', require('./components/Index/Textarea2'))
    Vue.component('detail-textarea2', require('./components/Detail/Textarea2'))
    Vue.component('form-textarea2', require('./components/Form/Textarea2'))

    Vue.component('index-image2', require('./components/Index/Image2'))
    Vue.component('detail-image2', require('./components/Detail/Image2'))
    Vue.component('form-image2', require('./components/Form/Image2'))
})
