let mix = require('laravel-mix')

mix.setPublicPath('dist')
    .js('resources/js/nova-fields.js', 'js')
    .sass('resources/sass/nova-fields.scss', 'css')
